package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
    	SportSocialNetworkUserImpl u1 = new SportSocialNetworkUserImpl<>("Super","Mario","Mario!", 10);
    	SportSocialNetworkUserImpl u2 = new SportSocialNetworkUserImpl<>("Andrea","Dovizioso","DesmoDovi");
    	SportSocialNetworkUserImpl u3 = new SportSocialNetworkUserImpl<>("Mark","Marquez","M93");
    
        u1.addSport(Sport.BASKET);
        u1.addSport(Sport.SOCCER);
        u1.addSport(Sport.TENNIS);
        u1.addSport(Sport.VOLLEY);
        
        u2.addSport(Sport.BIKE);
        u2.addSport(Sport.MOTOGP);
        
        u3.addSport(Sport.MOTOGP);
        u3.addSport(Sport.VOLLEY);
        
        System.out.println("TESTING ["+u1.getUsername()+"]");
        System.out.println("RESULT: "+u1.hasSport(Sport.SOCCER));
        
        System.out.println("TESTING ["+u2.getUsername()+"]");
        System.out.println("RESULT: "+u2.hasSport(Sport.MOTOGP));
        
        System.out.println("TESTING ["+u3.getUsername()+"]");
        System.out.println("RESULT: "+u3.hasSport(Sport.MOTOGP));
    }

}
