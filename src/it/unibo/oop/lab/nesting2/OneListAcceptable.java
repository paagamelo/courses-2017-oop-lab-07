package it.unibo.oop.lab.nesting2;

import java.util.*;

public class OneListAcceptable<T> implements Acceptable<T>{

	private final List<T> list;
	
	public OneListAcceptable(List<T> list) {
		this.list = list;
	}
	
	public Acceptor<T> acceptor() {
		return new AcceptorImpl<T>();
	}
	
	private class AcceptorImpl<T> implements Acceptor<T>{
		
		private final Set<T> set;
		
		private AcceptorImpl() {
			this.set = new HashSet(list);
		}
		
		public void accept(T newElement) throws ElementNotAcceptedException {
			if(set.contains(newElement)) {
				set.remove(newElement);
			} else {
				throw new ElementNotAcceptedException(newElement);
			}
		}

		public void end() throws EndNotAcceptedException {
			if(!set.isEmpty()) {
				throw new EndNotAcceptedException();
			}
		}
		
	}
}
